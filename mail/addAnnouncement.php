<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

?>
<span style="color: white">
<?= Html::img('http://www.agrobook24.pl/uploads/logo.png', ['style' => 'width:200px;']) ?>
<h3 style="color: white;">Gratulacje! Dodałeś nowe ogłoszenie.</h3>
Dziękujemy za zaufanie. Twoje ogłoszenie zostało dodane do bazy danych i oczekuje na akceptację moderatora. <br/>
Akceptacja może zająć do 24 godzin. Aby zmienić dodane informacje lub usunąć ogłoszenie proszę kliknąć w poniższy link: <br />
<?= Html::a('Edytuj ogłoszenie', Url::toRoute(['announcement/update', 'id' => $id, 'token' => $token], true),['style' => 'color: white; font-weight: bolder;'])?>

<br/><br/>
Pozdrawiamy,<br/>
Zespół <?= Html::a('agrobook24.pl', Url::home('true'),['style' => 'color: white; font-weight: bolder;'])?><br /><br/>
</span>