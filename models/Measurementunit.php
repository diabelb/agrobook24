<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "measurementunit".
 *
 * @property string $id
 * @property string $shortName
 * @property string $name
 *
 * @property Announcement[] $announcements
 */
class Measurementunit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measurementunit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shortName', 'name'], 'required'],
            [['shortName', 'name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shortName' => 'Short Name',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnnouncements()
    {
        return $this->hasMany(Announcement::className(), ['minimumOrderUnitId' => 'id']);
    }
}
