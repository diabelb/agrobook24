<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Announcement;

/**
 * AnnouncementSearch represents the model behind the search form about `app\models\Announcement`.
 */
class AnnouncementSearch extends Announcement {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'categoryId', 'subcategoryid', 'provinceId', 'minimumOrder', 'minimumOrderUnitId', 'priceUnitId'], 'integer'],
            [['title', 'content', 'date', 'imageFile', 'advertiser', 'phoneNumber', 'email'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Announcement::find();

        //$query->orderBy(['active DESC']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['active' => SORT_ASC, 'date' => SORT_DESC,]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categoryId' => $this->categoryId,
            'subcategoryid' => $this->subcategoryid,
            'date' => $this->date,
            'provinceId' => $this->provinceId,
            'minimumOrder' => $this->minimumOrder,
            'minimumOrderUnitId' => $this->minimumOrderUnitId,
            'price' => $this->price,
            'priceUnitId' => $this->priceUnitId,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'content', $this->content])
                ->andFilterWhere(['like', 'imageFile', $this->imageFile])
                ->andFilterWhere(['like', 'advertiser', $this->advertiser])
                ->andFilterWhere(['like', 'phoneNumber', $this->phoneNumber])
                ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }

}
