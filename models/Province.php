<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property string $id
 * @property string $name
 *
 * @property Announcement[] $announcements
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nazwa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnnouncements()
    {
        return $this->hasMany(Announcement::className(), ['provinceId' => 'id']);
    }
    
    public static function findAvailibleProvinces($announcementModel) {
        
        return static::find()->
                select(['province.id', 'province.name', 'announcement.categoryid', 'announcement.subcategoryid', 'announcement.minimumOrder', 'announcement.minimumOrderUnitId'])->
                rightJoin('announcement','announcement.provinceId = province.id')->
                groupBy('province.id')->
                orderBy('province.name')->
                andFilterWhere(['announcement.categoryid' => $announcementModel->categoryId])->
                andFilterWhere(['announcement.subcategoryid' => $announcementModel->subcategoryid])->
                andFilterWhere(['<=', 'announcement.minimumOrder', $announcementModel->minimumOrder])->
                andFilterWhere(['announcement.minimumOrderUnitId' => $announcementModel->minimumOrderUnitId])->
                asArray()->
                all();
    }
}
