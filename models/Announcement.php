<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "announcement".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $categoryId
 * @property integer $subcategoryid
 * @property string $date
 * @property string $imageFile
 * @property string $advertiser
 * @property integer $provinceId
 * @property string $phoneNumber
 * @property string $email
 * @property integer $minimumOrder
 * @property integer $minimumOrderUnitId
 * @property double $price
 * @property integer $priceUnitId
 * @property integer $active
 * @property string $token
 *
 * @property Category $category
 * @property Subcategory $subcategory
 * @property Province $province
 * @property Measurementunit $minimumOrderUnit
 * @property Measurementunit $priceUnit
 */
class Announcement extends \yii\db\ActiveRecord {

    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'announcement';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['active',], 'required'],
            ['title','required','message' => 'Proszę wpisać tytuł ogłoszenia.'],
            ['subcategoryid','required','message' => 'Proszę wybrać podkategorię.'],
            ['categoryId','required','message' => 'Proszę wybrać kategorię.'],
            ['content','required','message' => 'Proszę wpisać treść ogłoszenia.'],
            ['advertiser','required','message' => 'Proszę wpisać imię i nazwisko lub nazwę firmy.'],
            ['provinceId','required','message' => 'Proszę wybrać województwo.'],
            ['phoneNumber','required','message' => 'Proszę wpisać numer telefonu.'],
            ['email','required','message' => 'Proszę wpisać adres email.'],
            ['minimumOrder','required','message' => 'Proszę wpisać minimalne zamówienie.'],
            ['minimumOrderUnitId','required','message' => 'Proszę wybrać jednostkę.'],
            ['price','required','message' => 'Proszę wpisać cenę.'],
            ['priceUnitId','required','message' => 'Proszę wybrać jednostkę.'],
            
            
            
            [['title', 'content', 'advertiser', 'phoneNumber', 'email', 'token',], 'string'],
            [['categoryId', 'subcategoryid', 'provinceId', 'minimumOrder', 'minimumOrderUnitId', 'priceUnitId', 'active'], 'integer'],
            [['date'], 'safe'],
            [['price'], 'number'],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
            [['subcategoryid'], 'exist', 'skipOnError' => true, 'targetClass' => Subcategory::className(), 'targetAttribute' => ['subcategoryid' => 'id']],
            [['provinceId'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['provinceId' => 'id']],
            [['minimumOrderUnitId'], 'exist', 'skipOnError' => true, 'targetClass' => Measurementunit::className(), 'targetAttribute' => ['minimumOrderUnitId' => 'id']],
            [['priceUnitId'], 'exist', 'skipOnError' => true, 'targetClass' => Measurementunit::className(), 'targetAttribute' => ['priceUnitId' => 'id']],
            [['imageFile'], 'file', 'maxSize' => 1024 * 1024 * 5],
            [['image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Tytuł',
            'content' => 'Treść ogłoszenia',
            'categoryId' => 'Kategoria',
            'subcategoryid' => 'Podkategoria',
            'date' => 'Data',
            'imageFile' => 'Wstaw zdjęcie',
            'advertiser' => 'Imię i Nazwisko lub Nazwa Firmy',
            'provinceId' => 'Województwo',
            'phoneNumber' => 'Numer telefonu',
            'email' => 'Adres email',
            'minimumOrder' => 'Minimalne zamówienie',
            'minimumOrderUnitId' => 'Jednostka miary',
            'price' => 'Cena',
            'priceUnitId' => 'Jednostka miary',
            'active' => 'Active',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory() {
        return $this->hasOne(Subcategory::className(), ['id' => 'subcategoryid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince() {
        return $this->hasOne(Province::className(), ['id' => 'provinceId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinimumOrderUnit() {
        return $this->hasOne(Measurementunit::className(), ['id' => 'minimumOrderUnitId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceUnit() {
        return $this->hasOne(Measurementunit::className(), ['id' => 'priceUnitId']);
    }

    public static function findByModel($announcementModel) {
        return static::find()->
                        with('category')->
                        with('subcategory')->
                        with('province')->
                        with('minimumOrderUnit')->
                        with('priceUnit')->

                        andFilterWhere(['categoryId' => $announcementModel->categoryId])->
                        andFilterWhere(['subcategoryid' => $announcementModel->subcategoryid])->
                        andFilterWhere(['provinceId' => $announcementModel->provinceId])->
                        andFilterWhere(['<=', 'minimumOrder', $announcementModel->minimumOrder])->
                        andFilterWhere(['minimumOrderUnitId' => $announcementModel->minimumOrderUnitId]);
    }

}
