<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "announcement".
 *
 * @property string $id
 * @property string $title
 * @property string $content
 * @property string $categoryId
 * @property string $subcategoryid
 * @property string $date
 * @property string $imageFile
 * @property string $advertiser
 * @property string $provinceId
 * @property string $phoneNumber
 * @property string $email
 * @property integer $minimumOrder
 * @property string $minimumOrderUnitId
 * @property string $price
 * @property string $priceUnitId
 *
 * @property Category $category
 * @property Subcategory $subcategory
 * @property Province $province
 * @property Measurementunit $minimumOrderUnit
 * @property Measurementunit $priceUnit
 */
class Announcement extends \yii\db\ActiveRecord {

    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'announcement';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'content', 'categoryId', 'subcategoryid', 'advertiser', 'provinceId', 'phoneNumber', 'email', 'minimumOrder', 'minimumOrderUnitId', 'price', 'priceUnitId'], 'required'],
            [['title', 'content', 'advertiser', 'phoneNumber', 'email'], 'string'],
            [['categoryId', 'subcategoryid', 'provinceId', 'minimumOrder', 'minimumOrderUnitId', 'priceUnitId'], 'integer'],
            [['date'], 'safe'],
            [['price'], 'number'],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
            [['subcategoryid'], 'exist', 'skipOnError' => true, 'targetClass' => Subcategory::className(), 'targetAttribute' => ['subcategoryid' => 'id']],
            [['provinceId'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['provinceId' => 'id']],
            [['minimumOrderUnitId'], 'exist', 'skipOnError' => true, 'targetClass' => Measurementunit::className(), 'targetAttribute' => ['minimumOrderUnitId' => 'id']],
            [['priceUnitId'], 'exist', 'skipOnError' => true, 'targetClass' => Measurementunit::className(), 'targetAttribute' => ['priceUnitId' => 'id']],
            [['imageFile'], 'file', 'maxSize' => 1024 * 1024 * 5],
            [['image'], 'safe'],
            ['active', 'default', 'value' => 0],
                //[['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Tytuł',
            'content' => 'Treść ogłoszenia',
            'categoryId' => 'Kategoria',
            'subcategoryid' => 'Podkategoria',
            'date' => 'Data',
            'imageFile' => 'Wstaw zdjęcie',
            'advertiser' => 'Imię i Nazwisko lub Nazwa Firmy',
            'provinceId' => 'Województwo',
            'phoneNumber' => 'Numer telefonu',
            'email' => 'Adres email',
            'minimumOrder' => 'Minimalne zamówienie',
            'minimumOrderUnitId' => 'Jednostka miary',
            'price' => 'Cena',
            'priceUnitId' => 'Jednostka miary',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory() {
        return $this->hasOne(Subcategory::className(), ['id' => 'subcategoryid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvince() {
        return $this->hasOne(Province::className(), ['id' => 'provinceId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinimumOrderUnit() {
        return $this->hasOne(Measurementunit::className(), ['id' => 'minimumOrderUnitId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceUnit() {
        return $this->hasOne(Measurementunit::className(), ['id' => 'priceUnitId']);
    }

}
