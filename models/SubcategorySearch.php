<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Subcategory;

/**
 * SubcategorySearch represents the model behind the search form about `app\models\Subcategory`.
 */
class SubcategorySearch extends Subcategory {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'categoryId',], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Subcategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        //$query->joinWith('category');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'categoryId' => $this->categoryId,
        ]);

        //$query->andFilterWhere(['like', 'category.name', $this->getAttribute('category.name')]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        //$dataProvider->sort->attributes['category.name'] = [
        //    'asc' => ['category.name' => SORT_ASC],
        //    'desc' => ['category.name' => SORT_DESC],
        //];

        return $dataProvider;
    }

    public function attributes() {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['category.name']);
    }

}
