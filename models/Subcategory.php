<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subcategory".
 *
 * @property string $id
 * @property string $name
 * @property string $categoryId
 *
 * @property Announcement[] $announcements
 * @property Category $category
 */
class Subcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'categoryId'], 'required'],
            [['name'], 'string'],
            [['categoryId'], 'integer'],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'categoryId' => 'Kategoria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnnouncements()
    {
        return $this->hasMany(Announcement::className(), ['subcategoryid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
    
    public static function findAvailibleSubcategories($announcementModel) {
        
        return static::find()->
                select(['subcategory.id', 'subcategory.name', 'announcement.categoryid', 'announcement.provinceId', 'announcement.minimumOrder', 'announcement.minimumOrderUnitId'])->
                rightJoin('announcement','announcement.subcategoryid = subcategory.id')->
                groupBy('subcategory.id')->
                orderBy('subcategory.name')->
                andFilterWhere(['announcement.categoryid' => $announcementModel->categoryId])->
                andFilterWhere(['announcement.provinceId' => $announcementModel->provinceId])->
                andFilterWhere(['<=', 'announcement.minimumOrder', $announcementModel->minimumOrder])->
                andFilterWhere(['announcement.minimumOrderUnitId' => $announcementModel->minimumOrderUnitId])->
                asArray()->
                all();
    }
}
