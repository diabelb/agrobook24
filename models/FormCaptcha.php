<?php

namespace app\models;

use Yii;
use app\controllers\SiteController;


class FormCaptcha extends \yii\base\Model
{
     /**
     * @var string
     */
    public $captcha;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        //$rules[] = ['captcha', 'required', 'message' => 'Proszę przepisać kod zabezpieczający.'];
        //$rules[] = ['captcha', 'captcha', 'message' => 'Kod zabezpieczający jest nieprawidłowy. Prosze spróbować ponownie.'];
        //$rules[] = ['verifyCode', 'captcha'];
        //$rules[] = ['captchaAction','site/captcha'];
        $rules[] = [['captcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LdhBiETAAAAAEsIWiBg8p6C6fFB89DcRvnOu5lW'];
        return $rules;
    }
}
