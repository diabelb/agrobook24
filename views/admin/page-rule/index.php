<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\PageRule */
/* @var $form ActiveForm */

$this->title = 'Modyfikuj regulamin';
$this->params['breadcrumbs'][] = ['label' => 'Admin', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Regulamin', 'url' => ['/admin/page-rule']];
?>
<div class="box">
    <div class="box-body">

        <div class="admin-pageRule-index">

            <?php $form = ActiveForm::begin(); ?>
            <?=
            $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 30],
                'preset' => 'full'
            ])->label('');
            ?>

            <div class="form-group">
            <?= Html::submitButton('Zapisz', ['class' => 'btn btn-primary']) ?>
            </div>
<?php ActiveForm::end(); ?>

        </div><!-- admin-pageRule-index -->
    </div><!-- /.box-body -->
</div><!-- /.box -->