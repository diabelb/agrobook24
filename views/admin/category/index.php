<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySeatch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategorie';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Lista kategorii</h3>
        <div class="box-tools pull-right">
            <?= Html::a('Dodaj kategorii', ['create'], ['class' => 'btn btn-success']) ?>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="category-index">

            <h1><?= ''//Html::encode($this->title)    ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'name:ntext',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                $options = [
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('yii', 'Czy jesteś pewien, że chcesz usunąć kategorię: ' . $model->name . '? Operacja ta jest nieodwracalna i spowoduje również usunięcie wszystkich '
                                            . 'ogłoszeń oraz podkategorii przypisanych do danej kategorii!'),
                                ];
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                            },
                                ]
                            ],
                        ],
                    ]);
                    ?>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
