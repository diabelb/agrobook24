<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use himiklab\thumbnail\EasyThumbnailImage;
?>



<div class="panel-body">
    <div class="row">
        <div class="col-xs-3">
            <a href="<?= Yii::getAlias('@web') . '/uploads/' . Html::encode($model->imageFile) ?>" data-lightbox="<?= Html::encode($model->imageFile) ?>" data-title="<?= $model->title ?>">

                <?php
                //echo '/../uploads/' . Html::encode($model->imageFile);

                echo EasyThumbnailImage::thumbnailImg(
                        'uploads/' . Html::encode($model->imageFile), 250, 250, EasyThumbnailImage::THUMBNAIL_INSET, ['alt' => $model->title, 'class' => 'img-thumbnail']
                );
                ?>

            </a>
        </div>
        <div class="col-xs-9">
            <div class="row" style="min-height: 70px;">
                <?= HtmlPurifier::process($model->content) ?>
            </div>
            <div class="row" style="margin-top: 5px; font-weight: bold; font-size: 12px; color: #333">
                <div class="col-xs-5">
                    <span class="glyphicon glyphicon-scale" aria-hidden="true" style="font-size: 15px; color: #331A00"></span> Minimalne zamówienie:  <?= HtmlPurifier::process($model->minimumOrder) ?><?= HtmlPurifier::process($model->minimumOrderUnit->shortName) ?>
                </div>
                <div class="col-xs-7"> 
                    PLN <?= $model->price ?> zł / <?= $model->priceUnit->shortName ?>
                </div>
            </div>
            <div class="row" style="margin-top: 5px; font-weight: bold; font-size: 12px; color: #007070">
                <div class="col-xs-5">
                    <span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size: 15px; color: #331A00"></span> <?= HtmlPurifier::process($model->advertiser) ?> 
                </div>
                <div class="col-xs-7">
                    <span class="badge" style="background-color: #331A00;"> <?= HtmlPurifier::process($model->province->name) ?></span>
                </div>
            </div>
            <div class="row" style="margin-top: 5px; font-weight: bold; font-size: 12px; color: #007070">
                <div class="col-xs-5">
                    <span class="glyphicon glyphicon-phone-alt" style="font-size: 15px; color: #331A00"> </span>  <?= HtmlPurifier::process($model->phoneNumber) ?>
                </div>
                <div class="col-xs-7">
                    <span class="glyphicon glyphicon-envelope" style="font-size: 15px; color: #331A00"> </span>  <?= HtmlPurifier::process($model->email) ?>
                </div>
            </div>
        </div>
    </div>
</div>


