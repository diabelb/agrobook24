<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="announcement-form">

    <?php
    $form = ActiveForm::begin([
                //'id' => 'login-form',
                'options' => ['enctype' => 'multipart/form-data'],
    ]);
    ?>

    <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'categoryId')->textInput() ?>

    <?= $form->field($model, 'subcategoryid')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'imageFile')->textarea(['rows' => 6]) ?>

    <?=
    $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'options' => [
            'multiple' => true
        ],
        'pluginOptions' => [
            'removeClass' => 'btn btn-danger',
            'showUpload' => false,
            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
            'initialPreview' => [
                Html::img("/uploads/{$model->imageFile}", ['class' => 'file-preview-image', 'alt' => 'The Moon', 'title' => 'The Moon']),
            ],
            'initialCaption' => $model->imageFile,
            'overwriteInitial' => true
        ]
    ]);
    ?>

    <?= $form->field($model, 'advertiser')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'provinceId')->textInput() ?>

    <?= $form->field($model, 'phoneNumber')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'email')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'minimumOrder')->textInput() ?>

    <?= $form->field($model, 'minimumOrderUnitId')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'priceUnitId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
