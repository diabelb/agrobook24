<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Announcements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="announcement-view">

    <p>
        <?= Html::a('Modyfikuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php /* echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title:ntext',
            'content:ntext',
            'categoryId',
            'subcategoryid',
            'date',
            'imageFile:ntext',
            'advertiser:ntext',
            'provinceId',
            'phoneNumber:ntext',
            'email:ntext',
            'minimumOrder',
            'minimumOrderUnitId',
            'price',
            'priceUnitId',
        ],
    ]) */?>
    
    <?= $this->render('../../announcement/_post', [
            'model' => $model,
        ]); ?>

</div>
