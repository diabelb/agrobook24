<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */

$this->title = 'Edytuj ogłoszenie: ' . $announcements->title;
$this->params['breadcrumbs'][] = ['label' => 'Announcements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $announcements->title, 'url' => ['view', 'id' => $announcements->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Edycja</h3>
        <div class="box-tools pull-right">
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="announcement-update" style="padding:20px;">
            <?=
            $this->render('../../add-announcement/_form', [
                        'announcements' => $announcements,
                        'categories' => $categories,
                        'catlist' => $catlist,
                        'provincelist' => $provincelist,
                        'unitlist' => $unitlist,
                        'formCaptha' => $formCaptha,
            ]);
            ?>

        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
