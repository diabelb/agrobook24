<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AnnouncementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ogłoszenia';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Lista ogłoszeń</h3>
        <div class="box-tools pull-right">
            <?= Html::a('Dodaj ogłoszenie', ['create'], ['class' => 'btn btn-success']) ?>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">

        <div class="announcement-index">

            <h1><?php // echo Html::encode($this->title)               ?></h1>
            <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

            <p>

            </p>

            <?php
            Pjax::begin(['timeout' => 3000]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'hover' => true,
                'export' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'expandAllTitle' => 'Rozwiń wszystkie',
                        'expandTitle' => 'Rozwiń',
                        'collapseTitle' => 'Zwiń',
                        'expandIcon' => '<span class="glyphicon glyphicon-expand"></span>',
                        'value' => function ($model, $key, $index, $column) {
                            return GridView::ROW_COLLAPSED;
                        },
                        'detailUrl' => Url::to(['admin/announcement/announcement-detail']),
                        'detailRowCssClass' => GridView::TYPE_DEFAULT,
                    //'expandOneOnly' => true,
                    //'pageSummary' => false,
                    ],
                    //'id',
                    'title:ntext',
                    //'content:ntext',
                    //'categoryId',
                    [
                        'attribute' => 'categoryId',
                        'value' => 'category.name',
                        'filter' => ArrayHelper::map(app\models\Category::find()->asArray()->all(), 'id', 'name'),
                        'label' => 'Kategoria',
                    ],
                    [
                        'attribute' => 'subcategoryid',
                        'value' => 'subcategory.name',
                        'filter' => ArrayHelper::map(app\models\Subcategory::find()->asArray()->all(), 'id', 'name'),
                        'label' => 'Podktegoria',
                    ],
                    //'subcategoryid',
                    // 'date',
                    // 'imageFile:ntext',
                    // 'advertiser:ntext',
                    // 'provinceId',
                    // 'phoneNumber:ntext',
                    'email:ntext',
                    // 'minimumOrder',
                    // 'minimumOrderUnitId',
                    // 'price',
                    // 'priceUnitId',
                    [
                        'attribute' => 'active',
                        'value' => function ($model, $key, $index, $column) {
                            return $model->active ? '<span class="badge bg-green" style="">zaakceptowano</span>' : '<span class="badge bg-red" style="">oczekuje</span>';
                        },
                        //'filter' => ArrayHelper::map(app\models\Subcategory::find()->asArray()->all(), 'id', 'name'),
                        'label' => 'Status',
                        'format' => 'html',
                        'width' => '70px',
                    ],
                    [
                        'attribute' => 'active',
                        'value' => function ($model, $key, $index, $column) {
                            return $model->active ? '' : Html::a('<span class="badge bg-green" style=""><i class="fa fa-check"></i></span>',Url::to(['accept', 'id' => $model->id, 'token' => $model->token]));//<span class="badge bg-green" style=""><i class="fa fa-check"></i></span></a>';
                        },
                        //'filter' => ArrayHelper::map(app\models\Subcategory::find()->asArray()->all(), 'id', 'name'),
                        'label' => 'Akceptuj',
                        'format' => 'html',
                        'width' => '70px',
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            Pjax::end();
            ?>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
