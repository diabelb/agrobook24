<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubcategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Podkategorie';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Lista podkategorii</h3>
        <div class="box-tools pull-right">
            <?= Html::a('Dodaj podkategorię', ['create'], ['class' => 'btn btn-success']) ?>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="subcategory-index">

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'name:ntext:Nazwa',
                    [
                        'attribute' => 'categoryId',
                        'value' => 'category.name',
                        'filter' => ArrayHelper::map(app\models\Category::find()->asArray()->all(), 'id', 'name'),
                        'label' => 'Kategoria',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                $options = [
                                    'data-method' => 'post',
                                    'data-confirm' => Yii::t('yii', 'Czy jesteś pewien, że chcesz usunąć podkategorię: ' . $model->name . '? Operacja ta jest nieodwracalna i spowoduje również usunięcie wszystkich '
                                            . 'ogłoszeń przypisanych do danej podkategorii!'),
                                ];
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                            },
                                ]
                            ],
                        ],
                    ]);
                    ?>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->