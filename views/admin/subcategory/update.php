<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subcategory */

$this->title = 'Modyfikuj podkategorię: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Podkategorie', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edycja';
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Edycja</h3>
        <div class="box-tools pull-right">
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="subcategory-update">

            <?=
            $this->render('_form', [
                'model' => $model,
                'catlist' => $catlist,
            ])
            ?>

        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
