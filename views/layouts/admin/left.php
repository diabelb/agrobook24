<?php
use app\models\Announcement;
?>

<aside class="main-sidebar">

    <section class="sidebar left">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=Yii::getAlias('@web')?>/uploads/anonuser.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->username?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <?php 
        $badge = 'Edycja <span class="badge bg-orange"> ' . Announcement::find()->where(['active' => '0'])->count() . '<span>';
        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    [
                        'label' => 'Ustawienia',
                        'icon' => 'fa fa-gears',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Regulamin', 'icon' => 'fa fa-file-code-o', 'url' => ['/admin/page-rule'],],
                        ],
                    ],
                    [
                        'label' => 'RBAC',
                        'icon' => 'fa fa-users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Użytkownicy', 'url' => ['/rbac/user'],],
                            ['label' => 'Powiązania', 'url' => ['/rbac/assignment'],],
                            ['label' => 'Role', 'url' => ['/rbac/role'],],
                            ['label' => 'Uprawnienia', 'url' => ['/rbac/permission'],],
                            ['label' => 'Ścieżki', 'url' => ['/rbac/route'],],
                            ['label' => 'Reguły', 'url' => ['/rbac/rule'],],
                        ],
                    ],
                    [
                        'label' => 'Menu',
                        'icon' => 'fa fa-tasks',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Kategorie', 'url' => ['/admin/category'],],
                            ['label' => 'Podkategorie', 'url' => ['/admin/subcategory'],],
                            ['label' => 'Województwa', 'url' => ['/admin/province'],],
                        ],
                    ],
                    [
                        'label' => 'Ogłoszenia',
                        'icon' => 'fa fa-tasks',
                        'url' => '#',
                        'items' => [
                            ['label' => $badge, 'url' => ['/admin/announcement/index'],'encode' => false],
                           // ['label' => $badge, 'url' => ['/admin/announcement/acceptation'],'encode' => false],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
