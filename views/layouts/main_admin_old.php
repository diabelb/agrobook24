<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use app\models\Announcement;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::csrfMetaTags() ?>
        <meta charset="<?= Yii::$app->charset ?>">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head(); ?>

    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="container " >
            <div class="row">
                <div class="col-xs-9">
                    <?php NavBar::begin(['brandLabel' => 'HETA.PL', 'options' => ['class' => 'navbar navbar-default navbar-fixed-top']]); ?>
                    <?=
                    Nav::widget([
                        'items' => [
                            ['label' => 'PANEL ADMINISTRACYJNY', 'url' => ['/admin/announcement/index'], 'active' => true,],
                            !Yii::$app->user->isGuest ? ['label' => 'WYLOGUJ', 'url' => ['/announcement/logout']] : '',
                        ],
                        //'route' => '/admin/',
                        'options' => ['class' => 'navbar-nav'],
                    ]);
                    ?>
                    <?php NavBar::end(); ?>
                </div>
            </div><br /><br /><br /><br />
            <div class="row">
                <div class="col-xs-2">
                    <span class="categoryText">Ustawienia</span> <br />
<?php
echo Nav::widget([
    'items' => [
        ['label' => 'Ogólne', 'url' => ['category/index3']],
        ['label' => 'Statystyki', 'url' => ['subcategory/index22']],
        ['label' => 'Złote myśli', 'url' => ['subcategory/index22']],
    ],
    'route' => Yii::$app->controller->id . '/index',
    //'route' => 'announcement/index',
    //'params' => ['categoryid'],
    //'options' => ['class' =>'btn-group-vertical', 'role' => 'group']
    //'options' => ['role' => 'navigation']
    'options' => ['class' => 'nav-pills nav-stacked categoryLinks', 'role' => 'navigation'],
]);
?>
                    <br />
                    <?php if (Yii::$app->user->can('admin-rbac')) { ?>
                        <span class="categoryText">RBAC</span> <br />
                        <?php
                        echo Nav::widget([
                            'items' => [
                                ['label' => 'Użytkownicy', 'url' => ['/rbac/user'], 'active' => Yii::$app->controller->id == 'user' ? true : false],
                                ['label' => 'Powiązania', 'url' => ['/rbac/assignment'], 'active' => Yii::$app->controller->id == 'assignment' ? true : false],
                                ['label' => 'Role', 'url' => ['/rbac/role'], 'active' => Yii::$app->controller->id == 'role' ? true : false],
                                ['label' => 'Uprawnienia', 'url' => ['/rbac/permission'], 'active' => Yii::$app->controller->id == 'permission' ? true : false],
                                ['label' => 'Ścieżki', 'url' => ['/rbac/route'], 'active' => Yii::$app->controller->id == 'route' ? true : false],
                                ['label' => 'Reguły', 'url' => ['/rbac/rule'], 'active' => Yii::$app->controller->id == 'rule' ? true : false],
                            ],
                            //'route' => Yii::$app->controller->id.'/index',
                            //'route' => 'announcement/index',
                            //'params' => ['categoryid'],
                            //'options' => ['class' =>'btn-group-vertical', 'role' => 'group']
                            //'options' => ['role' => 'navigation']
                            'options' => ['class' => 'nav-pills nav-stacked categoryLinks', 'role' => 'navigation'],
                        ]);
                        ?>
                        <br />
                    <?php } ?>
                    <span class="categoryText">MENU</span> <br />
                    <?php
                    echo Nav::widget([
                        'items' => [
                            ['label' => 'Kategorie', 'url' => ['/admin/category/index']],
                            ['label' => 'Podkategorie', 'url' => ['/admin/subcategory/index']],
                            ['label' => 'Województwa', 'url' => ['/admin/province/index']],
                        ],
                        'route' => Yii::$app->controller->id . '/index',
                        //'route' => 'announcement/index',
                        //'params' => ['categoryid'],
                        //'options' => ['class' =>'btn-group-vertical', 'role' => 'group']
                        //'options' => ['role' => 'navigation']
                        'options' => ['class' => 'nav-pills nav-stacked categoryLinks', 'role' => 'navigation'],
                    ]);
                    ?>
                    <br />
                    <span class="categoryText">Ogłoszenia</span> <br />
                    <?php
                    $badge = 'Akceptuj<span class="badge">' . Announcement::find()->where(['active' => '0'])->count() . '<span>';
                    echo Nav::widget([
                        'encodeLabels' => false,
                        'items' => [
                            ['label' => 'Edycja', 'url' => ['/admin/announcement/index']],
                            ['label' => $badge, 'url' => ['/admin/announcement/acceptation'], 'encode' => false],
                            ['label' => 'Archiwum', 'url' => ['subcategory/index23']],
                            
                        ],
                        'route' => Yii::$app->controller->id . '/' .Yii::$app->controller->action->id,
                        //'route' => 'announcement/index',
                        //'params' => ['categoryid'],
                        //'options' => ['class' =>'btn-group-vertical', 'role' => 'group']
                        //'options' => ['role' => 'navigation']
                        'options' => ['class' => 'nav-pills nav-stacked categoryLinks', 'role' => 'navigation'],
                    ]);
                    ?>
                    <br />
                </div>
                <div class="col-xs-10">
<?= $content
?>
                </div>
            </div>
        </div>

<?php $this->endBody() ?>
    </body>
</html>
        <?php $this->endPage() ?>