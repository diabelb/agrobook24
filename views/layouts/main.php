<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use app\assets\AjaxAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= Html::csrfMetaTags() ?>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl . '/uploads'; ?>/favicon.ico" type="image/x-icon" />
        <title><?= Html::encode($this->title) ?></title>
        <meta name="keywords" content="giełda rolna, giełda, rolnictwo, owoce, warzywa, ziemniaki, zboża, ogłoszenia, sprzedam, kupię, rzepak, rolnik">

        <?php $this->head(); ?>
        <?php
        if (Yii::$app->user->isGuest) {
            AjaxAsset::register($this);
            ?>
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-55231959-4', 'auto');
                ga('send', 'pageview');

            </script>
<?php } ?>
    </head>
    <body>
<?php $this->beginBody() ?>

        <div class="container" id="container">

            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <?php NavBar::begin(['brandLabel' => '<img src="' . Yii::$app->request->baseUrl . '/uploads/logo.png" style="width: 160px; position: relative; bottom: 10px" />', 'options' => ['class' => 'navbar navbar-default navbar-fixed-top']]); ?>
                    <?=
                    Nav::widget([
                        'items' => [
                            ['label' => 'HOME', 'url' => ['announcement/index']],
                            ['label' => 'JAK TO DZIAŁA', 'url' => ['/announcement/how-does-it-work']],
                            ['label' => 'DODAJ OGŁOSZENIE', 'url' => ['/add-announcement/index'], 'options' => ['style' => 'background-color: #B02B2C;']],
                            !Yii::$app->user->isGuest ?
                                    [
                                'label' => '<img src="/uploads/anonuser.png" class="user-image" alt="User Image" style="width:20px; border-radius: 50%; margin-right: 10px; margin-top:-2px">' . mb_strtoupper(Yii::$app->user->identity->username),
                                'encodeLabels' => false,
                                'items' => [
                                    Yii::$app->user->can('admin') ? ['label' => 'PANEL ADMINISTRACYJNY', 'url' => ['/admin']] : '',
                                    ['label' => 'WYLOGUJ', 'url' => ['/announcement/logout']],
                                ],
                                    ] : '',
                        ],
                        'options' => ['class' => 'navbar-nav'],
                        'encodeLabels' => false,
                    ]);
                    ?>
                    <?php NavBar::end(); ?>
                </div>
            </div>
            <div id="middle">
                <?= $content
                ?>
            </div>

            <div id="footer" class="row">
                <span style="margin-right: 30px;"><?= Html::a("Regulamin", ['/announcement/rules']); ?></span>
                <span>Copyright © 2016 AgroBook24.pl</span>
            </div>
        </div>


        <?php $this->endBody() ?>
    </body>

</html>
<?php $this->endPage() ?>