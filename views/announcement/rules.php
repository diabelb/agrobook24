<?php

use yii\helpers\Html;

$this->title = 'agrobook24.pl - Giełda Rolna - Regulamin?';
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => 'Zobacz regulamin największej internetowej giełdy rolnej AgroBook24.pl. Z nami sprzedawanie staje się proste!',
]);
?>

<div class="rules title" ><h1>Regulamin</h1></div>
<?= $model->content; ?>