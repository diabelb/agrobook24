<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use himiklab\thumbnail\EasyThumbnailImage;
use app\assets\AnnouncementPostAsset;

AnnouncementPostAsset::register($this);
?>


<div class="post panel panel-default">
    <div class="panel-heading">
        <h2> <span class="glyphicon glyphicon-tags" aria-hidden="true"> </span> 
            <span class="tag"> <?= Html::a(mb_strtoupper(Html::encode($model->category->name), 'UTF-8'), yii\helpers\Url::to(['announcement/index', 'Announcement[categoryId]' => $model->category->id])) ?></span>
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="tag"><?= Html::a(mb_strtoupper(Html::encode($model->subcategory->name), 'UTF-8'), yii\helpers\Url::to(['announcement/index', 'Announcement[categoryId]' => $model->category->id, 'Announcement[subcategoryid][]' => $model->subcategory->id])) ?></span>

            <span class="postTitle"><?= Html::encode($model->title) ?></span>
        </h2>

    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 image">
                <a class="" href="<?= Yii::getAlias('@web') . '/uploads/' . Html::encode($model->imageFile) ?>" data-lightbox="<?= Html::encode($model->imageFile) ?>" data-title="<?= $model->title ?>">
                    <?php
                    echo EasyThumbnailImage::thumbnailImg(
                            'uploads/' . Html::encode($model->imageFile), 500, 500, EasyThumbnailImage::THUMBNAIL_INSET, ['alt' => $model->title, 'class' => 'img-thumbnail']
                    );
                    ?>
                </a>
            </div>
            <div class="col-xs-12 visible-xs-block"><br/></div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="row content">
                    <?= Html::encode($model->content) ?>
                </div>
                <div class="row detail">
                    <div class="col-md-5 col-xs-7">
                        <span class="glyphicon glyphicon-scale" aria-hidden="true"></span><span class="hidden-xs">Minimalne zamówienie: </span><span class="visible-xs-inline">Min. zamówienie: </span> <?= Html::encode($model->minimumOrder) ?><?= Html::encode($model->minimumOrderUnit->shortName) ?>
                    </div>
                    <div class="col-md-7 col-xs-5"> 
                        PLN <?= Html::encode($model->price) ?> zł / <?= $model->priceUnit->shortName ?>
                    </div>
                </div>
                <div class="row detail">
                    <div class="col-md-5 col-xs-7">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?= Html::encode($model->advertiser) ?> 
                    </div>
                    <div class="col-md-7 col-xs-5">
                        <span class="badge"> <?= Html::encode($model->province->name) ?></span>
                    </div>
                </div>
                <div class="row detail">

                    <div class="col-md-5 col-xs-7">
                        <span class="glyphicon glyphicon-envelope"> </span>  <?= Html::encode($model->email) ?>
                    </div>
                    <div class="col-md-7 col-xs-5">
                        <span class="glyphicon glyphicon-phone-alt"> </span>  <?= Html::encode($model->phoneNumber) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer"><?= \Yii::$app->formatter->asDate(Html::encode($model->date), 'full') ?></div>
</div>
