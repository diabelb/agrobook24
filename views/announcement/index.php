

<?php
/* @var $this yii\web\View */

use yii\bootstrap\Nav;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<?php
$this->title = 'agrobook24.pl - Giełda Rolna';

if ($announcementModel->categoryId) {
    $kategoria = \app\models\Category::findOne(['id' => $announcementModel->categoryId])->name;
    $this->title .= ' - ' . $kategoria;
    \Yii::$app->view->registerMetaTag([
        'name' => 'description',
        'content' => 'Zobacz produkty z kategorii ' . $kategoria . ' dostępne na internetowej giełdzie rolnej AgroBook24.pl. Z nami sprzedawanie staje się proste!',
    ]);
}
else {
    \Yii::$app->view->registerMetaTag([
        'name' => 'description',
        'content' => 'Największa internetowa giełda rolna. Jesteś producentem rolnym? Dodaj swoje produkty i sprzedaj je z zyskiem. Z nami to proste!',
    ]);
}

if ($announcementModel->subcategoryid) {
    $this->title .= ' - ' . \app\models\Subcategory::findOne(['id' => $announcementModel->subcategoryid])->name;
}



?>


<?php //Pjax::begin(['timeout' => 3000]);  ?>
<div class="row">
    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 hidden-xs">
        <span class="categoryText">KATEGORIE</span> <br />
        <?php
        echo Nav::widget([
            'items' => $menuItems,
            //'route' => 'announcement/index',
            //'params' => ['categoryid'],
            //'options' => ['class' =>'btn-group-vertical', 'role' => 'group']
            //'options' => ['role' => 'navigation']
            'options' => ['class' => 'nav-pills nav-stacked categoryLinks', 'role' => 'navigation'],
        ]);
        ?>
        <br />

        <div class="filter_announcements">

            <?php
            $form = ActiveForm::begin([
                        'action' => \yii\helpers\Url::current(['Announcement[categoryId]' => Yii::$app->request->Get('Announcement[categoryId]')]),
                        'enableClientValidation' => false,
                        'method' => 'get',
                        'options' => ['data-pjax' => '', 'class' => 'form-vertical fAnnouncements',],
            ]);
            ?>
            <?php
            if ($announcementModel->categoryId && $subcategoryMap) {

                echo $form->field($announcementModel, 'subcategoryid', [
                ])->checkboxList(
                        $subcategoryMap, [
                    //'enableLabel' => false,
                    'value' => true,
                    'onclick' => '$(".fAnnouncements").submit()',
                    'class' => 'categoryLinks',
                        ]
                )->label('PODKATEGORIE');
            }
            ?>

            <?php
            if ($provinceMap != []) {
                echo $form->field($announcementModel, 'provinceId')->checkboxList(
                        $provinceMap, [
                    'onclick' => '$(".fAnnouncements").submit()',
                    'class' => 'categoryLinks',
                        ]
                )->label('REGION');
            }
            ?>
            <label class="control-label" for="announcement-provinceid">FILTROWANIE</label><br /><br />

            <div class="row">
                <div class="col-lg-12 col-sm-12"><span class="menu_label categoryLinks">Minimalne zamówienie</span></div>
                <div class="col-lg-6 col-sm-6">
                    <?=
                            $form->field($announcementModel, 'minimumOrder')->textInput()
                            ->widget(MaskedInput::className(), [
                                'mask' => '9{1,8}',
                                'options' => ['style' => 'width:91px', 'class' => 'form-control'],
                                'class' => 'categoryLinks',
                            ])->label('');
                    ?>
                </div>
                <div class="col-lg-1 col-sm-6">
                    <?= $form->field($announcementModel, 'minimumOrderUnitId')->dropDownList($unitlist, ['prompt' => '-', 'style' => 'width:70px;'])->label(''); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-12">
                    <?= Html::submitButton('Filtruj ogłoszenia', ['class' => 'btn btn-primary filter-button']) ?>
                </div>
            </div>
            <?php
            ActiveForm::end();
            ?>
        </div>

    </div>
    <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12">
        <?php
        $session = Yii::$app->session;

        if ($session->hasFlash('message')) {
            ?>
            <div class="well" style="font-size: 12px; border-left: solid #8ebb5b 6px;">
                <span style="font-size: 14px; color: #3e6636; margin-bottom: 6px;"><strong>Gratulacje!</strong></span> <br /><?= $session->getFlash('message') ?>
            </div>
        <?php } ?>
        <div class="row visible-xs-block" style="padding: 20px;">
            <div class="dropdown" style="display: inline; float: left;">
                <button class="btn btn-default dropdown-toggle" style="font-size: 18px !important; margin-right: 10px;" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Kategoria
                    <span class="caret"></span>
                </button>
                <?php
                echo Nav::widget([
                    'items' => $menuItems,
                    //'route' => 'announcement/index',
                    //'params' => ['categoryid'],
                    //'options' => ['class' =>'btn-group-vertical', 'role' => 'group']
                    //'options' => ['role' => 'navigation']
                    'options' => ['class' => 'dropdown-menu', 'role' => 'navigation', 'aria-labelledby' => 'dropdownMenu1'],
                ]);
                ?>
            </div>
            <?php
            if ($announcementModel->categoryId && $subcategoryMap) {
                $form = ActiveForm::begin([
                            'action' => \yii\helpers\Url::current(['Announcement[categoryId]' => Yii::$app->request->Get('Announcement[categoryId]')]),
                            'enableClientValidation' => false,
                            'method' => 'get',
                            'options' => ['data-pjax' => '', 'class' => 'fAnnouncements2', 'style' => "display: inline; float: left;"],
                ]);
                ?>
                <?php
                $front = '<div class="dropdown" style="display: inline; float: left;">
                <button class="btn btn-success dropdown-toggle" style="font-size: 18px !important;" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Podkategoria
                    <span class="caret"></span>
                </button>';



                echo $form->field($announcementModel, 'subcategoryid', [
                    'inputTemplate' => $front . '{input}</div>',
                    'template' => '{input}',
                ])->checkboxList(
                        $subcategoryMap, [
                    //'enableLabel' => false,

                    'value' => true,
                    'onclick' => '$(".fAnnouncements2").submit()',
                    'class' => 'dropdown-menu nav',
                    'aria-labelledby' => 'dropdownMenu2',
                    'tag' => 'ul',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $js = 'if(!$(this).find(\'input\').is(\':checked\')) {'
                                . '$(this).find(\'input\').prop(\'checked\', true);'
                                . '} '
                                . 'else {'
                                . '$(this).find(\'input\').prop(\'checked\', false);'
                                . '}';
                        return '<li><a onclick="' . $js . '" href="#">
                                        <input type="checkbox" name="' . $name . '" value="' . $value . '" ' . ($checked ? 'checked' : '') . '/>' . $label . '
                                    </a></li>';
                    },
                        ]
                )->label('');
                ?>

                <?php
                ActiveForm::end();
            }
            ?>
        </div>

<?php
echo ListView::widget([
    'dataProvider' => $announcements,
    'itemView' => '_post',
    'summary' => '',
]);
?>



    </div>

</div>
<?php //Pjax::end();      ?>
