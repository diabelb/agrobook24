<?php

use yii\helpers\Html;

$this->title = 'agrobook24.pl - Giełda Rolna - Jak to działa?';
\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => 'Zobacz jak działa największa internetowa gieła rolna AgroBook24.pl. Z nami sprzedawanie staje się proste!',
]);
?>

<div class="howitworks title"><h1>Jak to działa?</h1></div><br/><br /><br />

<div class="row">

    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="col-lg-10 col-md-10">
            <div style="text-align: center; font-size: 17px; font-weight: bolder">Dodaj nowy produkt</div><br />
            <div style="text-align: center; font-size: 100px; color: #8ebb5b"><span class="glyphicon glyphicon-apple" aria-hidden="true"></span></div>
            <div style="text-align: center; font-size: 13px;">Wypełnij bezpłatny formularz zgłoszeniowy. Dodane ogłoszenie w każdej chwili możesz zmienić lub usunąć.</div>
        </div>
        <div class="col-lg-2 col-md-2 visible-lg-inline visible-md-inline">
            <div style="text-align: center; font-size: 40px; padding-top: 100px; color: #1F0000"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
        </div>

    </div>
    <div class="col-lg-2 col-md-12 col-sm-12 visible-sm-inline visible-xs-inline">
        <div style="text-align: center; font-size: 40px; padding-top: 20px; color: #1F0000"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="col-lg-10 col-md-12">
            <div style="text-align: center; font-size: 17px; font-weight: bolder">Weryfikacja ogłoszenia</div><br />
            <div style="text-align: center; font-size: 100px; color: #8ebb5b"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></div>
            <div style="text-align: center; font-size: 13px;">Moderator weryfikuje dodane ogłoszenie. Ogłoszenia niespójne, niekompletne nie są akceptowane.</div>
        </div>
        <div class="col-lg-2 visible-lg-inline">
            <div style="text-align: center; font-size: 40px; padding-top: 100px; color: #1F0000"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div>
        </div>
    </div>
    <div class="col-lg-2 col-md-12 visible-md-inline visible-sm-inline visible-xs-inline">
        <div style="text-align: center; font-size: 40px; padding-top: 20px; color: #1F0000"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div>
    </div>
    <div class="col-lg-4">
        <div class="col-lg-10">
            <div style="text-align: center; font-size: 17px; font-weight: bolder">Sprzedaż produktu</div><br />
            <div style="text-align: center; font-size: 100px; color: #8ebb5b"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span></div>
            <div style="text-align: center; font-size: 13px;">Zaineresowani kupnem kontaktują się z Tobą telefonicznie lub mailowo.</div>
        </div>        
    </div>
</div><br /><br /><br />
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-4" style="text-align: center;">
        <h3 style="font-size: 20px; font-weight: bolder">Jesteś producentem?</h3><br />
        <?=
        Html::a('Dodaj ogłoszenie', ['add-announcement/index'], [

            'class' => 'btn btn-green',
        ]);
        ?>
    </div>
    <div class="col-md-4" style="text-align: center;">
        <h3 style="font-size: 20px; font-weight: bolder">Zapoznaj się z regulaminem</h3><br />
        <?=
        Html::a('Regulamin', ['/announcement/rules'], [

            'class' => 'btn btn-orange',
        ]);
        ?>
    </div>
    <div class="col-md-2"></div>
</div>

