<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use yii\captcha\Captcha;
use kartik\widgets\DepDrop;
use kartik\widgets\FileInput;
use kartik\widgets\Select2;
?>
<?php
$form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data'],
        ])
?>
<?php if (Yii::$app->controller->id == "add-announcement") { ?>
    <div class="well" style="font-size: 12px; border-left: solid #8ebb5b 6px;">

        <span style="font-size: 14px; color: #3e6636; margin-bottom: 6px;"><strong>Dobra robota!</strong></span> <br />Już tylko kilka minut dzieli Cię od dodania własnego ogłoszenia. Ale pamiętaj! <strong>Nasz klient - nasz Pan.</strong> Dlatego postaraj się trochę.
        Ogłoszenia niespójne, niekompletne, z błędami - nie będą akceptowane. Szanujmy czas naszych klientów.
    </div>

<?php } ?>
<div class="row">
    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
        <?= $form->field($announcements, 'title')->textInput(['style' => 'width:95%']); ?>
    </div>
    <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">
        <?php //echo $form->field($announcements, 'categoryId')->dropDownList($catlist, ['prompt' => 'Wybierz', 'style' => 'width:150px']);
        ?>

        <?php
        echo $form->field($announcements, 'categoryId')->widget(Select2::classname(), [
            'data' => $catlist,
            'options' => ['placeholder' => 'Wybierz'],
            'pluginOptions' => [
                'width' => '95%',
            ]
        ]);
        ?>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <?=
        $form->field($announcements, 'subcategoryid')->widget(DepDrop::className(), [
            'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions' => [
                'depends' => [Html::getInputId($announcements, 'categoryId')], // the id for cat attribute
                'initialize' => isset($announcements->subcategoryid) ? true : false,
                'initDepends' => [Html::getInputId($announcements, 'categoryId')],
                'placeholder' => 'Wybierz',
                'url' => Url::to(['add-announcement/subcat', 'subCatId' => $announcements->subcategoryid]),
            ],
            'select2Options' => [
                'pluginOptions' => [
                    'width' => '95%',
                ]
            ],
        ]);
        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2 col-md-3 col-sm-8 col-xs-8">
        <?= $form->field($announcements, 'minimumOrder')->textInput(['style' => 'width:95%']); ?>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
        <?php
        echo $form->field($announcements, 'minimumOrderUnitId')->widget(Select2::classname(), [
            'data' => $unitlist,
            'options' => ['placeholder' => 'Wybierz'],
            'pluginOptions' => [
                'width' => '95%',
            ]
        ]);
        ?>
    </div>
    <div class="col-lg-4 col-md-2 visible-lg-inline visible-md-inline"></div>
    <div class="col-lg-2 col-md-3 col-sm-8 col-xs-8">
        <?=
        $form->field($announcements, 'price')->textInput()->widget(MaskedInput::className(), [
            //'mask' => '9{1,10}.99',
            'clientOptions' => [
                //'mask' => '9{1,10}.99',
                'alias' => 'currency',
                'prefix' => '',
                'groupSeparator' => '',
                'autoGroup' => true
            ],
            'options' => ['style' => 'width:95%', 'class' => 'form-control'],
        ])->label("Cena (zł)")
        ?>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
        <?php
        echo $form->field($announcements, 'priceUnitId')->widget(Select2::classname(), [
            'data' => $unitlist,
            'options' => ['placeholder' => 'Wybierz'],
            'pluginOptions' => [
                'width' => '95%',
            ]
        ]);
        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($announcements, 'content')->textarea(['style' => 'height:200px']); ?>

    </div>
    <div class="col-lg-12">
        <?php if (Yii::$app->controller->id == "add-announcement") { ?>

            <div class="well" style="font-size: 12px; border-left: solid #8ebb5b 6px;">

                Dodanie zdjęcia nie jest obowiązkowe. <strong>Ale cóż lepiej przyciągnie uwagę od genialnego zdjęcia,</strong> naszego jeszcze bardziej genialnego produktu?
            </div>
        <?php } ?>
        <?=
        $form->field($announcements, 'imageFile')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*', 'multiple' => false],
            'pluginOptions' => [
                'removeClass' => 'btn btn-danger',
                'showUpload' => false,
                'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                'initialPreview' => isset($announcements->imageFile) && !empty($announcements->imageFile) ?
                        [
                    Html::img("@web/uploads/{$announcements->imageFile}", ['class' => 'file-preview-image', 'alt' => $announcements->title, 'title' => $announcements->title]),
                        ] : '',
                'initialCaption' => isset($announcements->imageFile) ? $announcements->imageFile : '',
                'initialPreviewShowDelete' => true,
                'overwriteInitial' => true,
            ],
            'pluginEvents' =>
            [
                'fileclear' => "function(event, key) {
                    $('#announcement-image').val(\"delete\");
                }",
            ]
        ]);
        ?>
    </div>
</div>
<?php if (Yii::$app->controller->id == "add-announcement") { ?>
    <div class="well" style="font-size: 12px; border-left: solid #8ebb5b 6px;">

        <span style="font-size: 14px; color: #3e6636; margin-bottom: 6px;"><strong>Tak trzymaj!</strong></span> <br /> Już prawie skończyliśmy. Dodaj jeszcze tylko informacje o sobie - aby kontrahenci mogili się z Tobą skontaktować - <strong>i gotowe!</strong>
    </div>
<?php } ?>
<div class="row">
    <div class="col-lg-9 col-md-9">
        <?= $form->field($announcements, 'advertiser')->textInput(['style' => 'width:95%']); ?>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
        <?php
        echo $form->field($announcements, 'provinceId')->widget(Select2::classname(), [
            'data' => $provincelist,
            'options' => ['placeholder' => 'Wybierz'],
            'pluginOptions' => [
                'width' => '95%',
            ]
        ]);
        ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-2 col-md-3 col-sm-5 col-xs-5">
        <?=
        $form->field($announcements, 'phoneNumber')->widget(MaskedInput::className(), [
            'mask' => '999-999-999',
            'options' => ['style' => 'width:95%', 'class' => 'form-control'],
        ])
        ?>
    </div>   
    <div class="col-lg-3 col-md-4 col-sm-7 col-xs-7">
        <?=
        $form->field($announcements, 'email')->widget(MaskedInput::className(), [
            'clientOptions' => [
                'alias' => 'email'
            ],
            'options' => ['style' => 'width:95%', 'class' => 'form-control'],
        ])
        ?>
    </div>   
</div>

<?php echo $form->field($announcements, 'image')->hiddenInput()->label(false); ?>


<div class="row">
    <?php if (Yii::$app->controller->id == "add-announcement" || "announcement") { ?>
        <div class="col-lg-12">
            <?php //echo $form->field($formCaptha, 'captcha')->textInput()->widget(Captcha::className(), ['template' => '{image}{input}', 'options' => ['style' => 'width:300px', 'class' => 'form-control']]); ?>
            <?=
            $form->field($formCaptha, 'captcha')->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha::className(), ['siteKey' => '6LdhBiETAAAAAHpUZGg0cKLTjl16WA917caYrLk6']
            )->label('');
            ?>

        </div>
    <?php } ?>

    
        <div class="col-lg-12 col-xs-10">
            <?= Html::submitButton((Yii::$app->controller->id == "add-announcement" || "announcement") ? 'Dodaj' : 'Zapisz', ['class' => 'btn btn-primary']) ?>
            <?=
            Yii::$app->controller->id == "announcement" ?
                    Html::a('Usuń ogłoszenie', ['announcement/delete'], [
                        'data' => [
                            'method' => 'get',
                            'confirm' => 'Czy jesteś pewien, że chcesz usunąć ogłoszenie??',
                            'params' => ['id' => $announcements->id, 'token' => $announcements->token],
                        ],
                        'class' => 'btn btn-primary'
                    ]) : '';
            ?>
        </div>
    
</div>


<?php ActiveForm::end() ?>
