<?php
/* @var $this yii\web\View */
/* @var $categories \app\models\Category */

use yii\bootstrap\Nav;
$this->title = 'agrobook24.pl - Giełda Rolna - Dodaj ogłoszenie'; 
\Yii::$app->view->registerMetaTag([
        'name' => 'description',
        'content' => 'Dodaj nowy produkt i bądź widoczny w największej internetowej giełdzie rolnej AgroBook24.pl. Z nami sprzedawanie staje się proste!',
    ]);
?>
<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-2 visible-lg-inline visible-md-inline visible-sm-inline">
        <span class="categoryText">KATEGORIE</span> <br />
        <?php
        echo Nav::widget([
            'items' => $menuItems,
            //'route' => 'announcement/index',
            //'params' => ['categoryid'],
            //'options' => ['class' =>'btn-group-vertical', 'role' => 'group']
            //'options' => ['role' => 'navigation']
            'options' => ['class' => 'nav-pills nav-stacked categoryLinks', 'role' => 'navigation'],
        ]);
        ?>
        
    </div>
    <div class="col-xs-12 visible-xs-inline"><br/></div>
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">

        <?=
        $this->render('_form', [
            'announcements' => $announcements,
            'categories' => $categories,
            'catlist' => $catlist,
            'provincelist' => $provincelist,
            'unitlist' => $unitlist,
            'formCaptha' => $formCaptha,
        ]);
        ?>


    </div>
</div>