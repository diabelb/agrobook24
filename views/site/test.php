<?php
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div id="test1">
<?php Pjax::begin(['clientOptions' => ['container' => '#test2']]); ?>
<?= Html::a('Profile', ['site/test2', 'id' => 'testLink'], ['class' => 'profile-link']) ?>

<?php Pjax::end(); ?>
</div>
<div id="test2" test2="">
    <?php Pjax::begin(['id' => 'test2']); ?>
    test2
    <?php Pjax::end(); ?>
</div>
