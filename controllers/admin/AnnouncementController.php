<?php

namespace app\controllers\admin;

use Yii;
use app\models\Announcement;
use app\models\AnnouncementSearch;
use app\models\Category;
use app\models\Province;
use app\models\Measurementunit;
use app\models\FormCaptcha;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * AnnouncementController implements the CRUD actions for Announcement model.
 */
class AnnouncementController extends Controller {

    public $layout = 'admin/main_admin';
    public $actionParams = ['test' => 'aaaaaaa'];

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'announcement-detail'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'announcement-detail'],
                        'allow' => true,
                        'roles' => ['admin-announcement']
                    ],
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin-announcement-modify']
                    ],
                ]
            ],
        ];
    }

    /**
     * Lists all Announcement models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AnnouncementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Announcement model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionAnnouncementDetail() {
        if (isset($_POST['expandRowKey'])) {
            $model = $this->findModel($_POST['expandRowKey']);
            return $this->renderAjax('_post', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }

    /**
     * Creates a new Announcement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Announcement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Announcement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $formCaptha = new FormCaptcha();
        $oldImage = $model->imageFile;

        if (\Yii::$app->request->post()) {
            if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

                if (isset($model->imageFile)) {
                    $fileName = hash('md5', $model->imageFile->baseName . date('Y-m-d H:i:s')) . '.' . $model->imageFile->extension;
                    $model->imageFile->saveAs('uploads/' . $fileName);
                    $model->imageFile = $fileName;
                } else {
                    if ($model->image != 'delete') {
                        $model->imageFile = $oldImage;
                    } else {
                        $model->imageFile = 'no_image.png';
                        if ($oldImage != 'no_image.png') {
                            unlink(Yii::$app->basePath . '/web/uploads/' . $oldImage);
                        }
                    }
                }
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            //print_r($model);
        } else {
            $categories = new Category();
            $provinces = new Province();
            $measurementUnit = new Measurementunit();
            $catlist = $categories->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
            $provincelist = $provinces->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
            $unitlist = $measurementUnit->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
            $formCaptha = new FormCaptcha();


            return $this->render('update', [
                        'announcements' => $model,
                        'categories' => $categories,
                        'catlist' => $catlist,
                        'provincelist' => $provincelist,
                        'unitlist' => $unitlist,
                        'formCaptha' => $formCaptha,
            ]);
        }
    }

    /**
     * Deletes an existing Announcement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->imageFile != 'no_image.png') {
            unlink(Yii::$app->basePath . '/web/uploads/' . $model->imageFile);
        }
        $model->delete();

        return $this->redirect(['index']);
    }
    
    public function actionAcceptation() {
        $announcements = new ActiveDataProvider([
            'query' => Announcement::find()->andFilterWhere(['active' => false]),
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => ['defaultOrder' => ['date' => SORT_DESC]]
        ]);
        
        return $this->render('acceptation', [
                    'announcements' => $announcements,
                    'test'  => 'aaaaa',
        ]);
    }
    
    public function actionAccept() {
        if (\Yii::$app->request->get()) {
            $model = Announcement::findOne(['id' => Yii::$app->request->get('id')]);
            if(isset($model->id) && $model->token == Yii::$app->request->get('token')) {
                $model->active = 1;
                $model->save();
                return $this->redirect(['index']);
            }
        }
    }

    /**
     * Finds the Announcement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Announcement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Announcement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
