<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use app\models\Announcement;
use app\models\Subcategory;
use app\models\Province;
use app\models\Measurementunit;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\PageRules;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use \app\models\FormCaptcha;

class AnnouncementController extends \yii\web\Controller {

    public function actionIndex() {

        $added = Yii::$app->request->Get('added');
        //return Url::current();
        $announcementModel = new Announcement();
        $announcementModel->load(Yii::$app->request->Get());

        $measurementUnit = new Measurementunit();
        $unitlist = $measurementUnit->find()->select(['shortName', 'id'])->indexBy('id')->column();

        //--------------------KATEGORIE---------------------------
        $categories = Category::find()->orderBy('name')->all();

        foreach ($categories as $c) {
            $menu[] = array('label' => $c->name, 'url' => ['announcement/index', 'Announcement[categoryId]' => $c->id], 'active' => $announcementModel->categoryId == $c->id ? true : false);
        }

        $subcategoryMap = ArrayHelper::map(Subcategory::findAvailibleSubcategories($announcementModel), 'id', 'name');
        $provinceMap = ArrayHelper::map(Province::findAvailibleProvinces($announcementModel), 'id', 'name');

        //$query->andFilterWhere(['active' => 1]);

        $announcements = new ActiveDataProvider([
            'query' => Announcement::findByModel($announcementModel)->andFilterWhere(['active' => 1]), //->joinWith(['subcategory', 'category', 'province']),
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => ['defaultOrder' => ['date' => SORT_DESC]]
        ]);

        return $this->render('index', [
                    'announcements' => $announcements,
                    'menuItems' => $menu,
                    'subcategoryMap' => $subcategoryMap,
                    'added' => $added,
                    'announcementModel' => $announcementModel,
                    'provinceMap' => $provinceMap,
                    'unitlist' => $unitlist,
        ]);
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['admin-rbac']
                    ],
                ]
            ],
                /* [
                  'class' => 'yii\filters\PageCache',
                  'only' => ['index'],
                  'duration' => 0,
                  //         'varyByRoute' => false,
                  //      'dependency' => [
                  //        'class' => 'yii\caching\DbDependency',
                  //        'sql' => 'SELECT MAX(date) FROM announcement',
                  //],
                  'variations' => [
                  md5(Url::current() . (Yii::$app->user->isGuest ? 'aguest' : 'alogin')),
                  //$_GET['Announcement']['subcategoryid'],
                  //Yii::$app->request->Get(),
                  //(isset($_GET['categoryid'])) ? $_GET['categoryid'] : '',
                  //(isset($_GET['Announcement'])) ? $_GET['Announcement'] : '',
                  //(isset($_GET['id']))? $_GET['id'] : ''
                  ]
                  ], */
        ];
    }

    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Announcement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $token) {
        $model = $this->findModel($id);
        if ($model->token != $token) {
            throw new \yii\web\NotFoundHttpException();
        }

        $formCaptha = new FormCaptcha();
        $oldImage = $model->imageFile;

        if (\Yii::$app->request->post() && $formCaptha->load(\Yii::$app->request->Post()) && $formCaptha->validate()) {
            if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

                if (isset($model->imageFile)) {
                    $fileName = hash('md5', $model->imageFile->baseName . date('Y-m-d H:i:s')) . '.' . $model->imageFile->extension;
                    $model->imageFile->saveAs('uploads/' . $fileName);
                    $model->imageFile = $fileName;
                } else {
                    if ($model->image != 'delete') {
                        $model->imageFile = $oldImage;
                    } else {
                        $model->imageFile = 'no_image.png';
                        if ($oldImage != 'no_image.png') {
                            unlink(Yii::$app->basePath . '/web/uploads/' . $oldImage);
                        }
                    }
                }
                $model->active = 0;
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            }
            //print_r($model);
        } else {
            $categories = new Category();
            $provinces = new Province();
            $measurementUnit = new Measurementunit();
            $catlist = $categories->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
            $provincelist = $provinces->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
            $unitlist = $measurementUnit->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
            $formCaptha = new FormCaptcha();


            return $this->render('../add-announcement/_form', [
                        'announcements' => $model,
                        'categories' => $categories,
                        'catlist' => $catlist,
                        'provincelist' => $provincelist,
                        'unitlist' => $unitlist,
                        'formCaptha' => $formCaptha,
            ]);
        }
    }

    /**
     * Deletes an existing Announcement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $token) {
        $model = $this->findModel($id);

        if ($model->token != $token) {
            throw new \yii\web\NotFoundHttpException();
        }
        if ($model->imageFile != 'no_image.png') {
            unlink(Yii::$app->basePath . '/web/uploads/' . $model->imageFile);
        }
        $model->delete();


        return $this->redirect(['index']);
    }
    
    public function actionHowDoesItWork() {
        return $this->render('howdoesitwork');
    }

    public function actionRules() {
        $model = PageRules::find()->one();
        return $this->render('rules', [
                        'model' => $model,
            ]);
    }
    
    /**
     * Finds the Announcement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Announcement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Announcement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actions() {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

}
