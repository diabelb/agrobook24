<?php

namespace app\controllers;

use \app\models\Announcement;
use \app\models\Category;
use \app\models\Province;
use \app\models\FormCaptcha;
use \app\models\Measurementunit;
use \yii\helpers\Url;
use app\models\Subcategory;
use yii\helpers\Json;
use yii\web\UploadedFile;
use Yii;

class AddAnnouncementController extends \yii\web\Controller {

    public function actionIndex() {
        $categories = Category::find()->orderBy('name')->all();

        foreach ($categories as $c) {
            $menu[] = array('label' => $c->name, 'url' => ['announcement/index', 'categoryid' => $c->id]);
        }


        $announcements = new Announcement();
        $categories = new Category();
        $provinces = new Province();
        $measurementUnit = new Measurementunit();
        $catlist = $categories->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
        $provincelist = $provinces->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
        $unitlist = $measurementUnit->find()->select(['name', 'id'])->indexBy('id')->orderBy('name')->column();
        $formCaptha = new FormCaptcha();

        if (\Yii::$app->request->post()) {
            if ($announcements->load(\Yii::$app->request->post()) && $formCaptha->load(\Yii::$app->request->Post()) && $formCaptha->validate()) {
                $announcements->active = 0;
                $announcements->token = \Yii::$app->security->generateRandomString(20);
                if ($announcements->validate()) {
                    $announcements->imageFile = UploadedFile::getInstance($announcements, 'imageFile');

                    if (isset($announcements->imageFile)) {
                        $fileName = hash('md5', $announcements->imageFile->baseName . date('Y-m-d H:i:s')) . '.' . $announcements->imageFile->extension;
                        $announcements->imageFile->saveAs('uploads/' . $fileName);

                        $announcements->imageFile = $fileName;
                    } else {
                        $announcements->imageFile = 'no_image.png';
                    }
                    $announcements->save(false);

                    $this->sendMail($announcements->id, $announcements->token, $announcements->email);
                    
                    $session = Yii::$app->session;
                    $session->setFlash('message', 'Twoje ogłoszenie zostało dodane do bazy danych i oczekuje na akceptację moderatora. <br /> '
                            . 'Jeśli chcesz zmienić lub usunąć dodane ogłoszenie, sprawdź wiadomośc wysłaną na podany adres email: ' . $announcements->email);
                    $this->redirect(Url::toRoute(['announcement/index']));
                }
            }
        }


        return $this->render('index', [
                    'announcements' => $announcements,
                    'categories' => $categories,
                    'catlist' => $catlist,
                    'provincelist' => $provincelist,
                    'unitlist' => $unitlist,
                    'formCaptha' => $formCaptha,
                    'menuItems' => $menu
        ]);
    }

    public function actionSubcat() {

        if (\Yii::$app->request && ( \Yii::$app->request->isPost)) {
            $categoryId = \Yii::$app->request->Post()['depdrop_parents'];
            $subCat = Subcategory::find()->asArray()->select(['id', 'name'])->where(['categoryid' => $categoryId])->orderBy('name')->all();
            //if (\Yii::$app->request->isGet)
            $subCatId = \Yii::$app->request->get('subCatId');
            echo Json::encode(['output' => $subCat, 'selected' => isset($subCatId) ? $subCatId : '']);
            return;
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function sendMail($id, $token, $to) {
        Yii::$app->mailer->compose('addAnnouncement', [
                    'id' => $id,
                    'token' => $token,
                ])
                ->setFrom(['info@agrobook24.pl' => 'agrobook24.pl'])
                ->setTo($to)
                ->setSubject('Gratulacje! Dodałeś nowe ogłoszenie.')
                ->send();
    }

    //public function actions() {
    //    return [
    //        'captcha' => [
    //            'class' => 'yii\captcha\CaptchaAction',
    //        ],
    //    ];
    //}
}
